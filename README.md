# python_mouse_autoclicker

basierend auf Python 3.8

erfoderliche Module:
+ pynput
+ time
+ threading
+ tkinter
+ random


#

Funktionsweise:

nach dem starten des Programmes ( je nach Plattform etwas in der Art:     python3 python_mouse_autoclicker.py   )
ist es möglich mit speziellen mouse Tasten ( im Standard die beiden Daumentasten seitlich an der Maus, falls vorhanden) einen dauer - klicker zu starten und zu stoppen.

Einfach gesagt, ein Klick auf die vordere Daumentaste startet ein anhaltendes klicken der linken Maus Taste. Ein erneuter Klick auf dieselbe Daumentaste stoppt die Aktion wieder.




Das Programm bietet eine schlichte Benutzeroberfläche zum Einstellen der Klick-Geschwindigkeit.
Weitere individuelle Einstellungen (Sprache, Tasten) sind im Programmcode möglich.


überraschenderweise ist die Zuordnung ( Benennung ) der Mouse-Buttons auf einem Linux Manjaro/Ubuntu  System anders als auf einem Windows 10 Rechner. Während Linux auf ein ".button9" wartet ist es bei Windows ein ".button.x3". Ich befürchte in dieser Programm-Version muss es händisch im Code angepasst werden ;-).


Dieses Programm ist mein erster Versuch in Python, viele Elemente stammen aus diversen Online-Hilfe Seiten und sind teilweise kopiert und angepasst. Ich gestehe das ich manche Bereich davon gar nicht verstehe ( threading ). Auf jeden Fall war es eine angenehme Erfahrung in Python zu programmieren. Ich gehe nicht davon aus das ich das Projekt weiter verfolgen werde.



Ich hoffe es ist euch von nutzen, zum spielen oder als Vorlage.
agapetus