# short script to check which mouse buttons we have
# start the script in the terminal with something like:  python3 check_mouse_button.py


from pynput import mouse

def on_move(x, y):
    print('Pointer moved to {0}'.format(
        (x, y)))

def on_click(x, y, button, pressed):
    print(button)  # Print button to see which button of mouse was pressed
    print('{0} at {1}'.format(
        'Pressed' if pressed else 'Released',
        (x, y)))
    



# Collect events until released
with mouse.Listener(
        on_click=on_click
       ) as listener:
    listener.join()

# ...or, in a non-blocking fashion:
listener = mouse.Listener(on_click=on_click)
listener.start()