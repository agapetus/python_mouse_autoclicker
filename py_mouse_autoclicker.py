import time
import threading
from pynput.mouse import Button as pynbutton, Controller as pyncontroller, Listener as MouseListener
from pynput.keyboard import Listener as KeyListener, Key
from tkinter import *
import random
 
root = Tk()
root.title('Kana Apsa')
root.configure(bg='black')


def delaytime(val):
    delayed = float('{:.3f}'.format(1/(int(val)+1)))
    #print('delay:  ',delay)
    return delayed


# define all variables in class cargo:
class cargo():
    default_scale_left = 10
    default_scale_right = 8
    left_d = delaytime(default_scale_left) # adjust the default
    right_d = delaytime(default_scale_right) # adjust the default
    left_button = pynbutton.left #left mouse button
    right_button = pynbutton.right # right mouse button
    stop_key = Key.end # stop programm with keystroke: END
    left_start_stop_button = pynbutton.button9 # mouse button 9, front thumb button
#    left_start_stop_button = pynbutton.x2 # mouse button 9, front thumb button, WINDOWS?
    right_start_stop_button = pynbutton.button8 # mouse button 8, rear thumb button
#    right_start_stop_button = pynbutton.x1 # mouse button 8, rear thumb button, WINDOWS?
    topelements = 2 #0 =  nothing,  1 = images + help, 2 = canvas + help
    #image1_path = "images/1.png"
    #image2_path = "images/2.png"
    text_quitbutton = "ENDE"
    text_sliderbox = "feinjustieren der Mouse Buttons"
    text_sliderbox_left = "links"
    text_sliderbox_right = "rechts"
    text_mainlabel = "Superguter python3 Autoclicker!"
    text_help = """

Hilfe: 
-> Button9 startet und stoppt den Schnell-clicker LINKS
-> Button8 startet und stoppt den Schnell-clicker RECHTS
-> Taste END oder Button QUIT beendet das Programm

"""


def close():
    left_click_thread.exit()
    right_click_thread.exit()
    mouse_listener.stop()
    key_listener.stop()
#    mouse_listener.stop()
    root.destroy()
    return False 

def click_color(side):
    side.configure(background=random.choice(['black', 'red' , 'green' , 'blue' , 'yellow' , 'grey' , 'white' , 'magenta' , 'violet' ]))


def color_box(side,row,col):
    side = Canvas(root,
                  width=100,
                  height=100,
                  bg='black',
                  highlightthickness=3,
                  highlightbackground='red'
                 )
    side.grid(row=row,column=col, padx=20, pady=20)
    side.create_oval(25, 25, 75, 75, fill='red', outline='#DDD', width=4)
    return side




#-----------------------------------
# sliders

slidersbox = LabelFrame(root,
                     text= cargo.text_sliderbox,
                     padx=50,
                     bg= 'black',
                     fg='red')
slidersbox.grid(row=7,column=2,sticky='wn', padx=30, pady=30)
Message(slidersbox,
      bg='black',
      fg = 'red',
      font=('times', 18, 'italic')
      ).grid(row=1,column=1,sticky='wn')
slidersbox.config (bg='black')



class slider():
  
    def __init__ (self,Titel, mousebutton, default, rowX, columnY):
        def soll(val):
            showdelay = '{:.3f}'.format(1/int(val))
            self.Rechteck.config(text=showdelay)
            if mousebutton == 1:
                cargo.left_d = delaytime(val)
            elif mousebutton == 2:
                cargo.right_d = delaytime(val)


        self.regler = Scale (
                            slidersbox,
                            from_=1, 
                            to=30,
                            #variable=mydefault,
                            orient=HORIZONTAL,
                            #tickinterval=1,
                            length=300,
                            width=30,
                            bg='black',
                            bd=4,
                            font=('Arial', 18), 
                            fg='red',
                            sliderlength=50,
                            troughcolor='black',
                            command=soll)
        self.regler.grid(row=rowX+1,column=columnY,sticky='wn')
        self.regler.set(default)
        
        self.Rechteck = Label (
                                slidersbox,
                                font=('Courier', 18),
                                fg='red',
                                bg='black')
        self.Rechteck.grid(row=rowX+2,column=columnY,sticky='wn')
        
        self.Titel = Label (
                            slidersbox, 
                            text=Titel,
                            font=('Courier', 20),
                            bg='black', 
                            fg='red')
        self.Titel.grid(row=rowX,column=columnY,sticky='wn')


#---------------------------------------------

# wait and do with mouse input
def on_click(x, y, button, pressed):
  # start_stop_key will stop clicking 
  # if running flag is set to true
    x = x
    y = y
    if button == cargo.left_start_stop_button and pressed:
        if left_click_thread.running:
            left_click_thread.stop_clicking()
            print('left stop clicking')
        else:
            left_click_thread.start_clicking()
            print('left start clicking')
    elif button == cargo.right_start_stop_button and pressed:
        if right_click_thread.running:
            right_click_thread.stop_clicking()
            print('right stop clicking')
        else:
            right_click_thread.start_clicking()
            print('right start clicking')

#wait and do with  keyboard input
def on_press(key):
    print(key)    
    # here exit method is called and when 
    # key is pressed it terminates auto clicker
    if key == cargo.stop_key:
        close()


# threading.Thread is used 
# to control clicks
class leftClickMouse(threading.Thread):
    
  # delay and button is passed in class 
  # to check execution of auto-clicker
    def __init__(self, delay, button):
        super(leftClickMouse, self).__init__()
#        self.delay = cargo.left_d
        self.delay = delay
        self.button = button
        self.running = False
        self.program_running = True
  
    def start_clicking(self):
        self.running = True
  
    def stop_clicking(self):
        self.running = False
  
    def exit(self):
        self.stop_clicking()
        self.program_running = False
  
    # method to check and run loop until 
    # it is true another loop will check 
    # if it is set to true or not, 
    # for mouse click it set to button 
    # and delay.
    def run(self):
        while self.program_running:
            while self.running:
                mouse.click(self.button)
                time.sleep(cargo.left_d)
                click_color(left)
            time.sleep(cargo.left_d)



# threading.Thread is used 
# to control clicks
class rightClickMouse(threading.Thread):
    
  # delay and button is passed in class 
  # to check execution of auto-clicker
    def __init__(self, delay, button):
        super(rightClickMouse, self).__init__()
        self.delay = delay
        self.button = button
        self.running = False
        self.program_running = True
  
    def start_clicking(self):
        self.running = True
  
    def stop_clicking(self):
        self.running = False
  
    def exit(self):
        self.stop_clicking()
        self.program_running = False
  
    # method to check and run loop until 
    # it is true another loop will check 
    # if it is set to true or not, 
    # for mouse click it set to button 
    # and delay.
    def run(self):
        while self.program_running:
            while self.running:
                mouse.click(self.button)
                time.sleep(cargo.right_d)
                click_color(right)
            time.sleep(cargo.right_d)
  
  
# instance of mouse controller is created
mouse = pyncontroller()
left_click_thread = leftClickMouse(cargo.left_d, cargo.left_button)
left_click_thread.start()
right_click_thread = rightClickMouse(cargo.right_d, cargo.right_button)
right_click_thread.start()  
  



# some gui things, like text, button...
class mygui():

    mainlabel = Label(root,
          text=cargo.text_mainlabel,
          justify=LEFT,
          padx = 16
          )

    texthelp = Message(root, 
                   text = cargo.text_help,
                   width=800,
                   bg='black',
                   fg='red',
                   font=('times', 18, 'italic')
                   )

    quitbutton = Button(root,
                        text=cargo.text_quitbutton,
                        fg='red',
                        bd=5,
                        #bg='red',
                        relief='groove',
                        activebackground='red',
                        #highlightthickness=5,
                        highlightcolor='red',
                        command=close
                        )
'''
    img1 = PhotoImage(file=cargo.image1_path)
    myimg1 = Label(root,
          text='',
          bd=0,
          image = img1
          )

    img2 = PhotoImage(file=cargo.image2_path)
    myimg2 = Label(root,
          text='',
          bd=0,
          image = img2
          )
'''
    canvas1 = Canvas(root, 
                     width=200,
                     height=300,
                     bg='black',
                     bd=0,
                     highlightthickness=0
                     )
#   canvas1.create_oval(25, 25, 75, 75, fill='red', outline='#DDD', width=4)
#    canvas1.create_line(10, 5, 200, 50, fill='red', width=4)
#    canvas1.create_bitmap(50,50, bitmap='warning')
    canvas1.create_polygon(40,50,
                           160,50,
                           40,250,
                           160,250,
                           outline='red', width=4)

    canvas2 = Canvas(root, 
                     width=200,
                     height=300,
                     bg='black',
                     bd=0,
                     highlightthickness=0              
                     )
    canvas2.create_polygon(40,50,
                           160,50,
                           40,250,
                           160,250,
                           outline='red', width=4)

#---------------------------------------------------
# placing gui elements, ... depending on cargo settings

#create two color changing boxes
left = color_box('left',7,1)
right = color_box('right',7,4)

#create two slider. left and right
scale_left = slider(cargo.text_sliderbox_left,1,cargo.default_scale_left,1,1)
scale_right = slider(cargo.text_sliderbox_right,2,cargo.default_scale_right,1,2)





if cargo.topelements == 0:
    mygui.mainlabel.grid(row=1,column=1,columnspan=4)
    mygui.quitbutton.grid(row=2,column=2, ipadx=10, ipady=10, padx=10, pady=10)
elif cargo.topelements == 1:
    mygui.mainlabel.grid(row=1,column=1,columnspan=4)
    mygui.quitbutton.grid(row=3,column=2, ipadx=10, ipady=10, padx=10, pady=10)
    mygui.myimg1.grid(row=2,column=1, padx=20, pady=20)
    mygui.myimg2.grid(row=2,column=4, padx=20, pady=20)
    mygui.texthelp.grid(row=2,column=2,columnspan=2, padx=10, pady=10)
elif cargo.topelements == 2:
    mygui.mainlabel.grid(row=1,column=1,columnspan=4)
    mygui.quitbutton.grid(row=3,column=2, ipadx=10, ipady=10, padx=10, pady=10)
    mygui.canvas1.grid(row=2,column=1, padx=30, pady=30)
    mygui.canvas2.grid(row=2,column=4, padx=30, pady=30)
    mygui.texthelp.grid(row=2,column=2,columnspan=2, padx=10, pady=10)



# start listener, end page, stop listener
key_listener = KeyListener(on_press=on_press)
mouse_listener = MouseListener(on_click=on_click)
key_listener.start()
mouse_listener.start()

root.mainloop()

key_listener.stop()
mouse_listener.stop()
key_listener.join()
mouse_listener.join()



